﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using shopping_Version2.Customers.Dto;
using shopping_Version2.Customers;

namespace shopping_Version2.Customers
{
    public class CustomerService : ICustomerService
    {
        private readonly IRepository<Customer> CustomerRepository;
        private readonly IObjectMapper objectmapper;

        public CustomerService(IRepository<Customer> CustomerRepository,IObjectMapper objectmapper)
        {
            this.CustomerRepository = CustomerRepository;
            this.objectmapper = objectmapper;
        }
        public CustomerDto GetCustomerById(int id)
        {
            var customer = CustomerRepository.Get(id);
            var customer_ = objectmapper.Map<CustomerDto>(customer);
            return customer_ ;
        }

        public List<CustomerDto> GetAllCustomers()
        {
            var all = CustomerRepository.GetAllList();
            var allcustomers = objectmapper.Map<List<CustomerDto>>(all);
            return allcustomers;
        }
    }
}
