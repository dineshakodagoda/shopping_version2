﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Customers.Dto
{
   public class CustomerDto:EntityDto<int>
    {
        public string Name { get; set; }

        public string phonenumber { get; set; }

        public string Address { get; set; }
    }
}
