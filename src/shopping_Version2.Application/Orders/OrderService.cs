﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.ObjectMapping;
using Microsoft.EntityFrameworkCore;
using shopping_Version2.OrderItems;
using shopping_Version2.Exceptions;
using shopping_Version2.OrderItems.Dto;
using shopping_Version2.Orders.Dto;
using shopping_Version2.Products;

namespace shopping_Version2.Orders
{
    public class OrderService : IOrderService
    {
        private readonly IRepository<Order> orderRepository;
        private readonly IObjectMapper objectmapper;
        private readonly IRepository<Product> productrepository;
        private readonly IRepository<OrderItem> orderitemrepository;
        private readonly IUnitOfWorkManager unitofwork;
        private readonly ProductService productservice;

        public OrderService(IRepository<Order> orderRepository, IObjectMapper objectmapper
            , IRepository<Product> productrepository, IRepository<OrderItem> orderitemrepository
            , IUnitOfWorkManager unitofwork, ProductService productservice)
        {
            this.orderRepository = orderRepository;
            this.objectmapper = objectmapper;
            this.productrepository = productrepository;
            this.orderitemrepository = orderitemrepository;
            this.unitofwork = unitofwork;
            this.productservice = productservice;
        }
        public void AddOrder(OrderDto order)
        {
            if (order == null)
                throw new OrderNotFoundException();

            using (var unitOfWork = unitofwork.Begin())
            {
                try
                {
                    foreach (var item in order.orderItems)
                    {
                        //assigning correct values for the added order
                        var id = item.productid;
                        var orderitem = productservice.GetProductById(id);
                        var price = Convert.ToInt32(orderitem.price);
                        item.UnitPrice = price;
                    }
                    //Updating the product quantity
                    productservice.UpdateProductQuantity(order.orderItems);
                    var OrderNew = objectmapper.Map<Order>(order);
                    orderRepository.Insert(OrderNew);
                    unitOfWork.Complete();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        }

        public void DeleteOrder(int id)
        {
            var orderdel = orderRepository.GetAll().AsNoTracking().Include(o => o.orderItems)
                                                 .FirstOrDefault(f => f.Id == id);
            if (orderdel == null)
                throw new OrderNotFoundException();
            using (var unitOfWork = unitofwork.Begin())
            {
                try
                {
                    productservice.UpdateProductQuantity(objectmapper.Map<List<OrderItemDto>>(orderdel.orderItems));
                    orderRepository.Delete(id);
                    unitOfWork.Complete();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void DeleteOrderLine(int id)
        {
            throw new NotImplementedException();
        }

        public List<OrderDto> getAllOrder()
        {
            var orders = orderRepository.GetAll();
            var allorders = objectmapper.Map<List<OrderDto>>(orders);
            return allorders;
        }

        public OrderDto GetOrderById(int id)
        {
            var order = orderRepository.Get(id);
            var order_ = objectmapper.Map<OrderDto>(order);
            return order_;
        }

        public void UpdateOrder(OrderUDto orderdto)
        {

            if (orderdto == null)
                throw new OrderNotFoundException();
            else if (orderdto.orderItems == null)
                throw new OrderItemNotFoundException();

            //begining of the delete item part
            using (var unitOfWork = unitofwork.Begin())
            {
                try
                {
                    var products = productrepository.GetAll().AsNoTracking().ToList();
                    List<OrderItemUDto> updateItemList = new List<OrderItemUDto>();
                    foreach (var items in orderdto.orderItems)
                    {
                        if (items.isDeleted)
                            orderitemrepository.Delete(items.Id);
                        if (!items.isDeleted)
                        {
                            var item = products.FirstOrDefault(p => p.Id == items.productid);
                            items.UnitPrice = Convert.ToInt32(item.Price);
                            updateItemList.Add(items);
                        }
                    }
                    orderdto.orderItems = updateItemList;
                    orderRepository.Update(objectmapper.Map<Order>(orderdto));
                    unitOfWork.Complete();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        }//end of the update function
    }
}
