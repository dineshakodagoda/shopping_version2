﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using shopping_Version2.MultiTenancy;

namespace shopping_Version2.Sessions.Dto
{
    [AutoMapFrom(typeof(Tenant))]
    public class TenantLoginInfoDto : EntityDto
    {
        public string TenancyName { get; set; }

        public string Name { get; set; }
    }
}
