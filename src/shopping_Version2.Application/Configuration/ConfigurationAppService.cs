﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using shopping_Version2.Configuration.Dto;

namespace shopping_Version2.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : shopping_Version2AppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
