﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Exceptions
{
    public class CustomerNotFoundException :Exception
    {
        public CustomerNotFoundException() :base("Select a Customer!")
        {

        }
    }
}
