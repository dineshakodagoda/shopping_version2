﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Exceptions
{
   public class QuantityAmountExceededException : Exception
    {
        public QuantityAmountExceededException() :base("Quantity amount should be less than 10")
        {

        }
    }
}
