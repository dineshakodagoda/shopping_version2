﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Exceptions 
{
    public class QuantityNotGivenException:Exception
    {
        public QuantityNotGivenException() : base("Quantity is  zero")
        {

        }
    }
}
