﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Exceptions
{
    public class OrderNotFoundException :Exception
    {
        public OrderNotFoundException() :base("order not found!")
        {

        }
    }
}
