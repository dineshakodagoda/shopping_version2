﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Exceptions
{
   public class OrderItemNotFoundException : Exception
    {
        public OrderItemNotFoundException():base("Order Item Not Found!")
        {

        }
    }
}
