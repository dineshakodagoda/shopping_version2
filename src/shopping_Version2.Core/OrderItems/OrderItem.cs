﻿using Abp.Domain.Entities;
using shopping_Version2.Exceptions;
using shopping_Version2.Orders;
using shopping_Version2.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace shopping_Version2.OrderItems
{
    public class OrderItem : Entity<int>
    {
      
        public int productid { get; protected set; }
        [ForeignKey(nameof(productid))]
        public virtual Product product { get; set; }
        public int orderid { get;protected set; }

        [ForeignKey(nameof(orderid))]
        public virtual Order order { get; set; }
        private int quantity;
        [Required]
        [Range(maximum: 10, minimum: 0, ErrorMessage = "Maximum amount is 10")]
        public int Quantity {
            get
            {
                return quantity;
            }
            protected set 
            {
                if (value <= 0)
                    throw new QuantityNotGivenException();
                else if (value > 10)
                    throw new QuantityAmountExceededException();
                else
                quantity = value;
            } 
        }
        public int UnitPrice { get;protected set; }

        public OrderItem(int _productid , int _orderid , int _Quantity , int _UnitPrice)
        {
            _productid = productid;
            _orderid = orderid;
            _Quantity = Quantity;
            _UnitPrice = UnitPrice;
        }

    }
}
