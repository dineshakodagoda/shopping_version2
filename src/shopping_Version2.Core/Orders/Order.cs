﻿using Abp.Domain.Entities;
using Abp.Timing;
using shopping_Version2.Customers;
using shopping_Version2.Exceptions;
using shopping_Version2.OrderItems;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace shopping_Version2.Orders
{
    public class Order : Entity<int>
    {
       
        [Required]
        public DateTime OrderDate { get; protected set; }
        private List<OrderItem> _orderItems;
        [Required]
        public List<OrderItem> orderItems { 
            get
            { 
                return _orderItems; 
            } 
            protected set
            {
                if (value.Count < 1)
                {
                    throw new QuantityNotGivenException();
                }
                _orderItems = value;
            } 
        }

        [Required(ErrorMessage = "select a customer")]
        public int customerId { get; protected set; }

        [ForeignKey(nameof(customerId))]
        public virtual Customer customer { get;set; }
        private int _customerId;
        public int CustomerId
        {
            get
            {
                return _customerId;
            }
            protected set
            {
                if (value <= 0)
                {
                    throw new CustomerNotFoundException();
                }
                _customerId = value;
            }
        }

        public Order(DateTime orderdate, List<OrderItem> orderitems, int customerid)
        {
            orderdate = Clock.Now;
            orderitems = orderItems;
            customerid = customerId;
        }
    }
}
