﻿using Abp.Domain.Entities;
using shopping_Version2.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.Products
{
    public class Product : Entity<int>
    {
        public string ProductName { get; set; }
        public string Description { get; set; }
        private int _Qunatity;
        public int Quantity {
            get 
            {
                return _Qunatity;
            }
            set
            {
                if (value < 0)
                    throw new QuantityNotGivenException();
                _Qunatity = value;
            } 
        }
        public decimal Price { get; set; }

        public Product(string _ProductName , string _Description , int _quantity)
        {
            _ProductName = ProductName;
            _Description = Description;
            _quantity = Quantity;
        }
    }
}
