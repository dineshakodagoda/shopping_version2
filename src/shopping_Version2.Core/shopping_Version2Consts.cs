﻿namespace shopping_Version2
{
    public class shopping_Version2Consts
    {
        public const string LocalizationSourceName = "shopping_Version2";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
