﻿using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using shopping_Version2.Authorization.Roles;
using shopping_Version2.Authorization.Users;
using shopping_Version2.Customers;
using shopping_Version2.MultiTenancy;
using shopping_Version2.OrderItems;
using shopping_Version2.Orders;
using shopping_Version2.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace shopping_Version2.EntityFrameworkCore
{
    public class ShoppingDbcontext : AbpZeroDbContext<Tenant, Role, User,ShoppingDbcontext>
    {
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Customer> Customers { get; set; }
        
        public ShoppingDbcontext(DbContextOptions<ShoppingDbcontext> options) :base(options)
        {

        }
    }
}
