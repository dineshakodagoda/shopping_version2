﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using shopping_Version2.Configuration;
using shopping_Version2.Web;

namespace shopping_Version2.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class shopping_Version2DbContextFactory : IDesignTimeDbContextFactory<ShoppingDbcontext>
    {
        public ShoppingDbcontext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ShoppingDbcontext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            shopping_Version2DbContextConfigurer.Configure(builder, configuration.GetConnectionString(shopping_Version2Consts.ConnectionStringName));

            return new ShoppingDbcontext(builder.Options);
        }
    }
}
