﻿
using shopping_Version2.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shopping_Version2.EntityFrameworkCore.Seed.Customers
{
    public class DefaultCustomerBuilder
    {
        private readonly ShoppingDbcontext context;

        public DefaultCustomerBuilder(ShoppingDbcontext context)
        {
            this.context = context;
        }
        public void Create()
        {
            CreateDefaultCustomer();
        }
        private void CreateDefaultCustomer()
        {
            if (!context.Customers.Any())
            {
                var defaultcustomers = new List<Customer> 
                {
                    new Customer{Name="Customer1", Address="Kandy" , phonenumber="0773423456"},
                    new Customer{ Name="Customer2", Address="Kandy" , phonenumber="0773333456"},
                    new Customer{Name="Customer3", Address="Kandy" , phonenumber="0773423456"},
                    new Customer{ Name="Customer4", Address="Kandy" , phonenumber="0773423456"}
                };
                context.Customers.AddRange(defaultcustomers);
            }

         
        }


    }
}
