﻿
using shopping_Version2.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace shopping_Version2.EntityFrameworkCore.Seed.Products
{
   public class DefaultProductBuilder
    {
        private readonly ShoppingDbcontext context;

        public DefaultProductBuilder(ShoppingDbcontext context)
        {
            this.context = context;
        }
        public void Create()
        {
            CreateDefaultProduct();
        }

        private void CreateDefaultProduct()
        {
            //Default product

            if (!context.Products.Any())
            {
                var defaultProducts = new List<Product>
                {
                    new Product{ ProductName="dhal", Description="this is dhal", Price=110 , Quantity=100},
                    new Product{ProductName="sugar", Description="this is sugar", Price=120 , Quantity=100},
                     new Product{ ProductName="salt", Description="this is salt", Price=130 , Quantity=100},
                     new Product{ ProductName="rice", Description="this is rice", Price=100 , Quantity=100},
                };

                context.Products.AddRange(defaultProducts);
            }
        }

    }
}
