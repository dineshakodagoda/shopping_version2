using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace shopping_Version2.Controllers
{
    public abstract class shopping_Version2ControllerBase: AbpController
    {
        protected shopping_Version2ControllerBase()
        {
            LocalizationSourceName = shopping_Version2Consts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
