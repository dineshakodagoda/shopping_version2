using Microsoft.AspNetCore.Antiforgery;
using shopping_Version2.Controllers;

namespace shopping_Version2.Web.Host.Controllers
{
    public class AntiForgeryController : shopping_Version2ControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
