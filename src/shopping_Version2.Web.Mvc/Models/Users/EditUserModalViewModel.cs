using System.Collections.Generic;
using System.Linq;
using shopping_Version2.Roles.Dto;
using shopping_Version2.Users.Dto;

namespace shopping_Version2.Web.Models.Users
{
    public class EditUserModalViewModel
    {
        public UserDto User { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }

        public bool UserIsInRole(RoleDto role)
        {
            return User.RoleNames != null && User.RoleNames.Any(r => r == role.NormalizedName);
        }
    }
}
