﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shopping_Version2.Web.Models.Order
{
    public class UpdateOrderLine
    {
        public int OrderItemId { get; set; }
        public int productid { get; set; }
        public int Quantity { get; set; }
        public int UnitPrice { get; set; }
        public bool isDeleted { get; set; }
    }
}
