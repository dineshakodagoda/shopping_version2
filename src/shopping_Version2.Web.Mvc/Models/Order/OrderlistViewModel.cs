﻿using shopping_Version2.Orders.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shopping_Version2.Web.Models.Order
{
    public class OrderlistViewModel
    {
        public List<OrderDto> orderlist { get; set; }
    }
}
