﻿using shopping_Version2.Products.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace shopping_Version2.Web.Models.Order
{
    public class updateorderviewmodel
    {
        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public List<orderlineViewmodel> orderItems { get; set; }
        public int customerId { get; set; }
        public List<ProductDto> productlist { get; set; }
    }
}
