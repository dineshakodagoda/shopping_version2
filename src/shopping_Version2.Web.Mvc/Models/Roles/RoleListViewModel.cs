﻿using System.Collections.Generic;
using shopping_Version2.Roles.Dto;

namespace shopping_Version2.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleListDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
