﻿
using shopping_Version2.OrderItems.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShoppingcartProj.ViewModel
{
    public class OrderViewModel
    {
       [Required]
       public int customerId { get; set; }
       public DateTime OrderDate { get; set; }

       [Required]
      public List<OrderItemDto> orderItems { get; set; }

    }
}
