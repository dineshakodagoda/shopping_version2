﻿using System.Collections.Generic;
using shopping_Version2.Roles.Dto;

namespace shopping_Version2.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}