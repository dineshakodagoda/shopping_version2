﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace shopping_Version2.Web.Views
{
    public abstract class shopping_Version2ViewComponent : AbpViewComponent
    {
        protected shopping_Version2ViewComponent()
        {
            LocalizationSourceName = shopping_Version2Consts.LocalizationSourceName;
        }
    }
}
